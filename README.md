# Soprt Mart

An e-commerce website built with Next.js


## [Live Application](https://sport-mart-a5b3g0sln-jakhi161265.vercel.app/)



![App Screenshot](sportMart-ss.png)


## Sample login credentials:

```bash
admin@example.com (Admin)
123456

user@example.com (User)
1234567
```

  
## Tech Stack

**Technologies:**
- React Js
- Next Js
- Node Js
- Material UI
- React Material UI Carousel
- React Hook Form
- Context API
- Mongoose
- JWT Authentication 
- Bcrypt Js
- Js Cookies
- React Social Icons

**Deployment:**
- Vercel

## Features
- Fully functional cart
- Product details, ratings
- Product search by name, filter by sport type and brand name
- Product sort by pricing(high to low and vise versa) and by rating(high to low and vise versa)
- JWT authentication
- User profile update
- User order management
- Admin role with authorization
- Admin order delivery management
- Admin order details page
- Admin and user both watch order history with details
- Shipping page
- Payment page
- Order details page
  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/jakhi161265/sport-mart
```

Go to the project directory

```bash
  cd sport-mart
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev
```

Seedind data

```bash
  https://sport-mart-a5b3g0sln-jakhi161265.vercel.app/api/seed
```


  
