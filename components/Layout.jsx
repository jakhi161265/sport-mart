import { AppBar, Toolbar, Typography, Grid, Badge, Button, Menu, MenuItem, InputBase, IconButton, Avatar } from '@mui/material'
import { Container } from '@mui/system'
import React, { useContext, useState } from 'react'
import NextLink from 'next/link'
import { styled } from '@mui/material/styles';
import Head from "next/head";
import { Store } from '../utils/Store';
import Cookies from 'js-cookie';
import { useRouter } from "next/router";
import StyledLink from './StyledLink';
import SearchIcon from '@mui/icons-material/Search';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import ArticleRoundedIcon from '@mui/icons-material/ArticleRounded';
import ExitToAppRoundedIcon from '@mui/icons-material/ExitToAppRounded';
import Footer from './Footer';
import { useSnackbar } from "notistack";
import PropTypes from "prop-types";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Fade from "@mui/material/Fade";

function ScrollTop(props) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            "#back-to-top-anchor"
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: "center",
            });
        }
    };

    return (
        <Fade in={trigger}>
            <Box
                onClick={handleClick}
                role="presentation"
                sx={{ position: "fixed", bottom: 16, right: 16 }}
            >
                {children}
            </Box>
        </Fade>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

const StyledAppBar = styled(AppBar)({
    backgroundColor: "#000000", // black shade
    '& a': {
        color: "#ffffff",
        marginLeft: 10,
    }
});

const StyledContainer = styled(Container)({
    minHeight: '50vh',
    marginBottom: "5rem"
});

const StyledTypography = styled(Typography)({
    fontWeight: "bold",
    fontSize: "1.5rem",
});


const StyledGrid = styled(Grid)({
    display: "flex",
    padding: "0rem",
    justifyContent: "center",
    alignItems: "center",
});

const StyledForm = styled("form")({
    border: '1px solid #ffffff',
    width: "30rem",
    backgroundColor: '#ffffff',
    borderRadius: 5,
});

const StyledInputBase = styled(InputBase)({
    // border: "1px solid orange",
    width: "27.8rem",
    paddingLeft: "1rem",
    color: '#000000',
    '& ::placeholder': {
        color: '#606060',
    },
});

const StyledAvatar = styled(Avatar)({
    width: 20, height: 20, padding: "3px", color: "#358856"
});

const StyledIconButton = styled(IconButton)({
    backgroundColor: '#358856',
    color: "white",
    padding: "0.3rem",
    borderRadius: '5px 0 0 5px',
    '& span': {
        color: '#000000',
    },
    '&:hover': {
        backgroundColor: "#757575",
        color: "white",
    },
})


export default function Layout(props) {
    const { title, description, children } = props
    const { state, dispatch } = useContext(Store)
    const { cart, userInfo } = state
    const router = useRouter()
    const [anchorEl, setAnchorEl] = useState(null);
    const [searchQuery, setSearchQuery] = useState('');
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const searchHandler = (e) => {
        e.preventDefault();
        router.push(`/search?query=${searchQuery}`);
    };

    const loginMenuCloseHandler = (e, redirect) => {
        setAnchorEl(null);
        if (redirect) {
            router.push(redirect);
        }
    };

    const logoutClickHandler = () => {
        closeSnackbar();
        setAnchorEl(null);
        dispatch({ type: 'USER_LOGOUT' });
        Cookies.remove('userInfo');
        Cookies.remove('cartItems');
        Cookies.remove('shippingAddress');
        Cookies.remove('paymentMethod');
        enqueueSnackbar(`Bye, ${userInfo.name} !!! Have A Good Day`, { variant: "success" });
        router.push('/');
    };

    return (
        <>
            <Head>
                <title>{title ? `${title} - Sport Mart` : "Sport Mart"}</title>
                {description && <meta name='description' content={description}></meta>}
            </Head>

            {/* header area */}
            <StyledAppBar position="static">
                <Toolbar id="back-to-top-anchor" >
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item md={4} xs={12} sx={{ paddingBottom: "1rem" }}>
                            <NextLink href="/" passHref>
                                <StyledLink>
                                    <StyledTypography>Sport Mart</StyledTypography>
                                </StyledLink>
                            </NextLink>
                        </Grid>

                        {/* search area */}
                        <Grid item md={4} xs={12}>
                            <StyledForm onSubmit={searchHandler}>
                                <StyledIconButton
                                    type="submit"
                                    aria-label="search"
                                >
                                    <SearchIcon />
                                </StyledIconButton>

                                <StyledInputBase
                                    name="query"
                                    placeholder="Search Product By Name"
                                    onChange={(e) => setSearchQuery(() => e.target.value)}
                                />
                            </StyledForm>
                        </Grid>

                        <StyledGrid item md={4} xs={12} sx={{ justifyContent: "flex-end" }}>
                            <NextLink href="/cart" passHref>
                                <StyledLink sx={{ marginRight: "0.5rem" }}>
                                    {cart?.cartItems?.length > 0 ? <Badge color="primary" badgeContent={cart.cartItems.length}><Typography><b>Cart</b></Typography></Badge> : <Typography><b>Cart</b></Typography>}
                                </StyledLink>
                            </NextLink>
                            {
                                userInfo ? (
                                    <>
                                        <Button aria-controls="simple-menu"
                                            aria-haspopup="true"
                                            onClick={(e) => setAnchorEl(e.currentTarget)} sx={{ textTransform: "initial" }}>
                                            <StyledAvatar>{userInfo.name[0]}</StyledAvatar>
                                        </Button>
                                        <Menu
                                            id="simple-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={Boolean(anchorEl)}
                                            onClose={loginMenuCloseHandler}
                                        >
                                            <MenuItem
                                                onClick={(e) => loginMenuCloseHandler(e, '/profile')}
                                            >
                                                <AccountCircleRoundedIcon sx={{ marginRight: "0.5rem" }} />  Profile
                                            </MenuItem>
                                            <MenuItem
                                                onClick={(e) =>
                                                    loginMenuCloseHandler(e, '/order-history')
                                                }
                                            >
                                                <ArticleRoundedIcon sx={{ marginRight: "0.5rem" }} />  Order Hisotry
                                            </MenuItem>

                                            <MenuItem onClick={logoutClickHandler}> <ExitToAppRoundedIcon sx={{ marginRight: "0.5rem" }} /> Logout</MenuItem>
                                        </Menu>
                                    </>) :
                                    <NextLink href="/login" passHref>
                                        <StyledLink>
                                            <Typography><b>Login</b></Typography>
                                        </StyledLink>
                                    </NextLink>
                            }
                        </StyledGrid>
                    </Grid>
                </Toolbar>
            </StyledAppBar>

            {/* body area */}
            <StyledContainer>{children}</StyledContainer>

            {/* footer area */}
            <footer>
                <Footer></Footer>
            </footer>

            <ScrollTop {...props}>
                <Fab size="small" aria-label="scroll back to top" sx={{ backgroundColor: "#358856", color: "white" }}>
                    <KeyboardArrowUpIcon />
                </Fab>
            </ScrollTop>
        </>
    )
}
