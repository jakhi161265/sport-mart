import React from "react";
import NextLink from "next/link";
import Image from "next/image";
import {
    TableContainer,
    Table,
    Typography,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Link,
} from "@mui/material";
import StyledLink from "../components/StyledLink";
import Rating from '@mui/material/Rating';

export default function ProductDetailsTable(props) {
    const { cartItems } = props
    console.log('item', cartItems);

    return (
        <> <TableContainer>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Image</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Rating</TableCell>
                        <TableCell align="right">Quantity</TableCell>
                        <TableCell align="right">Price</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {cartItems.map((item) => (
                        <TableRow key={item._id}>
                            <TableCell>
                                <NextLink href={`/product/${item.slug}`} passHref>
                                    <Link>
                                        <Image
                                            src={item.image}
                                            alt={item.name}
                                            width={50}
                                            height={50}
                                        ></Image>
                                    </Link>
                                </NextLink>
                            </TableCell>

                            <TableCell>
                                <NextLink href={`/product/${item.slug}`} passHref>
                                    <StyledLink>
                                        <Typography>{item.name}</Typography>
                                    </StyledLink>
                                </NextLink>
                            </TableCell>

                            <TableCell>
                                <Rating name="read-only" value={item?.rating} readOnly />
                            </TableCell>
                            <TableCell align="right">
                                <Typography>{item.quantity}</Typography>
                            </TableCell>
                            <TableCell align="right">
                                <Typography>${item.price}</Typography>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer></>
    )
}
