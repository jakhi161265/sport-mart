import { Grid } from "@mui/material";

export default function CarouselItem(props) {
    return (
        <Grid
            sx={{
                backgroundImage: `url(${props.item.image})`,
                backgroundSize: "cover",
                backgroundPosition: "center top",
                backgroundRepeat: "no-repeat",
                marginBottom: "3rem",
                height: "27rem",
                position: "relative",
            }}
        >
        </Grid>
    )
}
