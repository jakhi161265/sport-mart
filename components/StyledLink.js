import { Link } from "@mui/material";
import { styled } from "@mui/material/styles";

const StyledLink = styled(Link)({
  textDecoration: "none",
});


export default StyledLink;