import React from 'react'
import { List, Typography, Grid, ListItem } from '@mui/material'
import { styled } from '@mui/material/styles';
import NextLink from 'next/link'
import StyledLink from './StyledLink';
import { SocialIcon } from 'react-social-icons';

const StyledTitleTypography = styled(Typography)({
    fontWeight: "bold",
    fontSize: "1.5rem",
    color: "white",
});

const StyledSubTitleTypography = styled(Typography)({
    color: "white",
    fontSize: "1.2rem",
});

const StyledDescriptionTypography = styled(Typography)({
    color: "#757575"
});

const StyledFooter = styled(Typography)({
    display: "flex",
    color: "white",
    flex: 1,
    fontWeight: "bold",
    borderTop: "1px solid #eaeaea",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "1rem",
});

export default function Footer() {
    return (
        <>
            <Grid container
                sx={{ backgroundColor: "#000000", padding: "2rem 4rem" }}>
                <Grid item md={5} xs={12}>
                    <List>
                        <ListItem><NextLink href="/" passHref>
                            {/* place here a icon */}
                            <StyledLink>
                                <StyledTitleTypography>Sport Mart</StyledTitleTypography>
                            </StyledLink>
                        </NextLink></ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa sequi inventore labore provident soluta molestias repudiandae vero ipsum vitae dolor obcaecati quaerat neque, eveniet harum explicabo porro consequatur modi amet.
                            </StyledDescriptionTypography>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item md={3} xs={12}>
                    <List>
                        <ListItem>
                            <StyledSubTitleTypography>Our Location</StyledSubTitleTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>
                                G-11, DCC Market, Gulshan-2,
                                Dhaka-1212 Bangladesh
                            </StyledDescriptionTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography >
                                Mobile: 01711180619
                            </StyledDescriptionTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>
                                Email: sportMart@gmail.com
                            </StyledDescriptionTypography>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item md={2} xs={12}>
                    <List>
                        <ListItem>
                            <StyledSubTitleTypography>About Us</StyledSubTitleTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>Careers
                            </StyledDescriptionTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>
                                Terms &amp; Conditions
                            </StyledDescriptionTypography>
                        </ListItem>
                        <ListItem>
                            <StyledDescriptionTypography>
                                Corporate
                            </StyledDescriptionTypography>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item md={2} xs={12}>
                    <List>
                        <ListItem>
                            <StyledSubTitleTypography>Follow Us</StyledSubTitleTypography>
                        </ListItem>
                        <ListItem>
                            <SocialIcon network='facebook' fgColor="white" style={{ height: 25, width: 25, marginRight: "0.5rem" }} />
                            <SocialIcon network='twitter' fgColor="white" style={{ height: 25, width: 25, marginRight: "0.5rem" }} />
                            <SocialIcon network='youtube' fgColor="white" style={{ height: 25, width: 25, marginRight: "0.5rem" }} />
                            <SocialIcon network='instagram' fgColor="white" style={{ height: 25, width: 25, marginRight: "0.5rem" }} />
                            <SocialIcon network='snapchat' fgColor="white" style={{ height: 25, width: 25, marginRight: "0.5rem" }} />
                        </ListItem>

                    </List>
                </Grid>
                <StyledFooter>Copyright {new Date().getFullYear()} Sport Mart. All rights reserved.</StyledFooter>
            </Grid>

        </>
    )
}
