import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Stack,
    Typography,
} from '@mui/material'
import React from 'react';
import Rating from '@mui/material/Rating';
import { useRouter } from "next/router";
import AddShoppingCartRoundedIcon from '@mui/icons-material/AddShoppingCartRounded';
import VisibilityRoundedIcon from '@mui/icons-material/VisibilityRounded';

export default function ProductItem({ product, handleAddCart }) {
    const router = useRouter();
    return (
        <Card>
            <CardActionArea>
                <CardMedia
                    component="img"
                    image={product.image}
                    title={product.name}
                    height={250}
                    layout="responsive"
                ></CardMedia>

                <CardContent>
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        spacing={2}
                        sx={{ marginBottom: "1rem" }}
                    >
                        <Typography variant="h3" component="h3">
                            {product.name}
                        </Typography>
                        <Typography variant="h3" component="h3" color="primary">
                            <b> ${product.price}</b>
                        </Typography>
                    </Stack>
                    <Rating name="read-only" value={product.rating} readOnly />
                </CardContent>
            </CardActionArea>

            <CardActions sx={{ paddingBottom: "1rem", paddingTop: "0rem", display: "flex", justifyContent: "space-evenly" }}>
                <Button
                    size="small"
                    color="primary"
                    variant="contained"
                    onClick={() => handleAddCart(product)}
                    startIcon={<AddShoppingCartRoundedIcon />}
                    sx={{ padding: "0.2rem 0.5rem" }}
                >
                    Add To Cart
                </Button>

                <Button
                    size="small"
                    color="primary"
                    variant="outlined"
                    onClick={() => router.push(`/product/${product.slug}`)}
                    startIcon={<VisibilityRoundedIcon />}
                    sx={{ padding: "0.2rem 0.5rem" }}
                >
                    Quick View
                </Button>
            </CardActions>
        </Card>

    )
}
