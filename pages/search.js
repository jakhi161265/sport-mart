import { Button, Grid, MenuItem, Select, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useContext } from "react";
import Layout from "../components/Layout";
import db from "../utils/db";
import Product from "../models/Product";
import ProductItem from "../components/ProductItem";
import { Store } from "../utils/Store";
import axios from "axios";
import { useSnackbar } from "notistack";
import Image from "next/image";
import noSearchResultFound from "../public/images/noSearchResult.webp";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import dynamic from "next/dynamic";

function Search(props) {
  const router = useRouter();
  const { sort = "featured", category = "all", brand = "all" } = router.query;
  const { products, categories, brands } = props;

  const filterSearch = ({ sort, searchQuery, category, brand }) => {
    const path = router.pathname;
    const { query } = router;
    if (searchQuery) query.searchQuery = searchQuery;
    if (sort) query.sort = sort;
    if (category) query.category = category;
    if (brand) query.brand = brand;

    router.push({
      pathname: path,
      query: query,
    });
  };

  const categoryHandler = (e) => {
    filterSearch({ category: e.target.value });
  };

  const brandHandler = (e) => {
    filterSearch({ brand: e.target.value });
  };

  const sortHandler = (e) => {
    filterSearch({ sort: e.target.value });
  };

  const { state, dispatch } = useContext(Store);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleAddCart = async (product) => {
    closeSnackbar();
    const existItem = state.cart.cartItems.find((x) => x._id === product._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const { data } = await axios.get(`/api/products/${product._id}`);

    if (data.data.countInStock < quantity) {
      enqueueSnackbar("Sorry. Product is out of stock!!!!!!", {
        variant: "error",
      });
      return;
    }

    dispatch({ type: "CART_ADD_ITEM", payload: { ...product, quantity } });
    
    // if new product is added then show notification
    if (quantity === 1) {
      closeSnackbar();
      enqueueSnackbar("Product Added In The Cart", {
        variant: "success",
      });
    }

    // if quantity is greater than 1 then go to cart page
    if (quantity > 1) {
      router.push("/cart");
    }
  };

  return (
    <Layout title="Search">
      <Grid container spacing={1}>
        {products.length === 0 ? (
          <Grid container sx={{ textAlign: "center", marginTop: "1.5rem" }}>
            <Grid item xs={12} md={12} sx={{ marginBottom: "2rem" }}>
              <Image
                src={noSearchResultFound}
                alt={"no search found"}
                width={450}
                height={200}
              ></Image>
            </Grid>
            <Grid item xs={12} md={12}>
              <Button
                variant="contained"
                startIcon={<ArrowBackRoundedIcon />}
                onClick={() => router.push("/")}
              >
                Back To Home
              </Button>
            </Grid>
          </Grid>
        ) : (
          <Grid item md={12} xs={12}>
            <Grid container sx={{ marginTop: "1rem" }}>
              <Grid item md={4} xs={12}>
                <Typography component="span" sx={{ marginRight: "0.5rem" }}>
                  Sort By:
                </Typography>
                <Select value={sort} size="small" onChange={sortHandler}>
                  <MenuItem value="featured">Featured</MenuItem>
                  <MenuItem value="PriceLowest">Price: Low to High</MenuItem>
                  <MenuItem value="PriceHighest">Price: High to Low</MenuItem>
                  <MenuItem value="RatingLowest">Rating: Low to High</MenuItem>
                  <MenuItem value="RatingHighest">Rating: High to Low</MenuItem>
                </Select>
              </Grid>

              <Grid item md={4} xs={12}>
                <Typography component="span" sx={{ marginRight: "0.5rem" }}>
                  Sports:
                </Typography>
                <Select
                  size="small"
                  value={category}
                  onChange={categoryHandler}
                >
                  <MenuItem value="all">All</MenuItem>
                  {categories &&
                    categories.map((category) => (
                      <MenuItem key={category} value={category}>
                        {category}
                      </MenuItem>
                    ))}
                </Select>
              </Grid>

              <Grid item md={4} xs={12}>
                <Typography component="span" sx={{ marginRight: "0.5rem" }}>
                  Brands:
                </Typography>
                <Select size="small" value={brand} onChange={brandHandler}>
                  <MenuItem value="all">All</MenuItem>
                  {brands &&
                    brands.map((brand) => (
                      <MenuItem key={brand} value={brand}>
                        {brand}
                      </MenuItem>
                    ))}
                </Select>
              </Grid>
            </Grid>

            {products.length > 0 && (
              <Grid container spacing={3} sx={{ marginTop: "0rem" }}>
                {products.map((product) => (
                  <Grid item md={3} key={product.name}>
                    <ProductItem
                      product={product}
                      handleAddCart={handleAddCart}
                    />
                  </Grid>
                ))}
              </Grid>
            )}
          </Grid>
        )}
      </Grid>
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  await db.connect();
  const sort = query.sort || "";
  const searchQuery = query.query || "";
  const category = query.category || "";
  const brand = query.brand || "";

  const queryFilter =
    searchQuery && searchQuery !== "all"
      ? {
          name: {
            $regex: searchQuery,
            $options: "i",
          },
        }
      : {};

  const categoryFilter = category && category !== "all" ? { category } : {};
  const brandFilter = brand && brand !== "all" ? { brand } : {};

  const sortedResult =
    sort === "featured"
      ? { featured: -1 }
      : sort === "PriceLowest"
      ? { price: 1 }
      : sort === "PriceHighest"
      ? { price: -1 }
      : sort === "RatingLowest"
      ? { rating: 1 }
      : sort === "RatingHighest"
      ? { rating: -1 }
      : { _id: -1 };

  const productDocs = await Product.find({
    ...queryFilter,
    ...categoryFilter,
    ...brandFilter,
  })
    .sort(sortedResult)
    .lean();

  const categories = await Product.find().distinct("category");
  const brands = await Product.find().distinct("brand");
  const products = productDocs.map(db.convertDocToObj);

  return {
    props: {
      products,
      categories,
      brands,
    },
  };
}

export default dynamic(() => Promise.resolve(Search), { ssr: false });
