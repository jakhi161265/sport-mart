import axios from "axios";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import NextLink from "next/link";
import React, { useEffect, useContext, useReducer } from "react";
import {
  CircularProgress,
  Grid,
  List,
  ListItem,
  TableContainer,
  Typography,
  Card,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  ListItemText,
  Chip,
  IconButton,
  Tooltip,
} from "@mui/material";
import { getError } from "../utils/error";
import { Store } from "../utils/Store";
import Layout from "../components/Layout";
import AccountCircleRoundedIcon from "@mui/icons-material/AccountCircleRounded";
import ArticleRoundedIcon from "@mui/icons-material/ArticleRounded";
import { useSnackbar } from "notistack";
import GridViewRoundedIcon from "@mui/icons-material/GridViewRounded";
import CheckCircleRoundedIcon from "@mui/icons-material/CheckCircleRounded";

function reducer(state, action) {
  switch (action.type) {
    case "FETCH_REQUEST":
      return { ...state, loading: true, error: "" };
    case "FETCH_SUCCESS":
      return { ...state, loading: false, orders: action.payload, error: "" };
    case "FETCH_FAIL":
      return { ...state, loading: false, error: action.payload };
    case "DELIVER_REQUEST":
      return { ...state, loadingDeliver: true };
    case "DELIVER_SUCCESS":
      return { ...state, loadingDeliver: false, successDeliver: true };
    case "DELIVER_FAIL":
      return { ...state, loadingDeliver: false, errorDeliver: action.payload };
    default:
      state;
  }
}

function OrderHistory() {
  const { state } = useContext(Store);
  const router = useRouter();
  const { userInfo } = state;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [{ loading, orders }, dispatch] = useReducer(reducer, {
    loading: true,
    orders: [],
  });

  async function deliverOrderHandler(order) {
    closeSnackbar();
    try {
      dispatch({ type: "DELIVER_REQUEST" });
      const { data } = await axios.put(
        `/api/orders/${order._id}/deliver`,
        {},
        {
          headers: { authorization: `Bearer ${userInfo.token}` },
        }
      );
      dispatch({ type: "DELIVER_SUCCESS", payload: data });
      fetchOrders();
    } catch (err) {
      enqueueSnackbar("Fail to deliver", { variant: "error" });
      dispatch({ type: "DELIVER_FAIL", payload: getError(err) });
    }
  }

  const fetchOrders = async () => {
    try {
      dispatch({ type: "FETCH_REQUEST" });
      const { data } = await axios.get(`/api/orders/history`, {
        headers: { authorization: `Bearer ${userInfo.token}` },
      });
      dispatch({ type: "FETCH_SUCCESS", payload: data });
    } catch (err) {
      dispatch({ type: "FETCH_FAIL", payload: getError(err) });
    }
  };

  useEffect(() => {
    if (!userInfo) {
      router.push("/login");
    }

    fetchOrders();
  }, []);

  return (
    <Layout title="Order History">
      <Grid container spacing={2} sx={{ marginTop: "1rem" }}>
        <Grid item md={3} xs={12}>
          <Card>
            <List>
              <NextLink href="/profile" passHref>
                <ListItem button component="a">
                  <AccountCircleRoundedIcon sx={{ marginRight: "1rem" }} />
                  <ListItemText primary="User Profile"></ListItemText>
                </ListItem>
              </NextLink>
              <NextLink href="/order-history" passHref>
                <ListItem selected button component="a">
                  <ArticleRoundedIcon sx={{ marginRight: "1rem" }} />
                  <ListItemText primary="Order History"></ListItemText>
                </ListItem>
              </NextLink>
            </List>
          </Card>
        </Grid>
        <Grid item md={9} xs={12}>
          <Card>
            <List>
              <ListItem>
                <Typography component="h1" variant="h1">
                  Order History
                </Typography>
              </ListItem>
              <ListItem>
                {loading ? (
                  <CircularProgress />
                ) : orders.length > 0 ? (
                  <TableContainer>
                    <Table size="small">
                      <TableHead>
                        <TableRow>
                          <TableCell>ID</TableCell>
                          <TableCell>PLACED ORDER</TableCell>
                          <TableCell>TOTAL</TableCell>
                          <TableCell>PAID</TableCell>
                          <TableCell>DELIVERED</TableCell>
                          <TableCell align="right">ACTIONS</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {orders.map((order) => (
                          <TableRow key={order._id}>
                            <TableCell>{order._id.substring(18, 24)}</TableCell>
                            <TableCell>
                              {new Date(order.createdAt).toDateString()}
                            </TableCell>
                            <TableCell>${order.totalPrice}</TableCell>
                            <TableCell>
                              <Chip
                                sx={{
                                  color: "#FFFFFF",
                                  fontWeight: "bold",
                                  backgroundColor: order.isPaid
                                    ? "#43a047"
                                    : "#ff5252",
                                }}
                                size="small"
                                label={order.isPaid ? "Paid" : "Not Paid"}
                              ></Chip>
                            </TableCell>
                            <TableCell>
                              <Chip
                                size="small"
                                sx={{
                                  color: "#FFFFFF",
                                  fontWeight: "bold",
                                  backgroundColor: order.isPaid
                                    ? "#43a047"
                                    : "#ff5252",
                                }}
                                label={
                                  order.isDelivered
                                    ? "Delivered"
                                    : "Not Delivered"
                                }
                              ></Chip>
                            </TableCell>
                            <TableCell align="right">
                              <NextLink href={`/order/${order._id}`} passHref>
                                <Tooltip title="View Order Details">
                                  <IconButton color="primary">
                                    <GridViewRoundedIcon />
                                  </IconButton>
                                </Tooltip>
                              </NextLink>
                              {userInfo?.isAdmin && (
                                <Tooltip title="Confirm Delivery">
                                  <IconButton
                                    color="primary"
                                    disabled={order.isDelivered ? true : false}
                                    onClick={() => deliverOrderHandler(order)}
                                  >
                                    <CheckCircleRoundedIcon />
                                  </IconButton>
                                </Tooltip>
                              )}
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                ) : (
                  <Typography sx={{ color: "red", fontWeight: "bold" }}>
                    {" "}
                    Sorry, your history is empty !!! Visit {""}
                    <span>
                      <NextLink href="/" passHref  sx={{color:"black"}}>
                        Home
                      </NextLink>
                    </span>{" "}
                    to make history
                  </Typography>
                )}
              </ListItem>
            </List>
          </Card>
        </Grid>
      </Grid>
    </Layout>
  );
}

export default dynamic(() => Promise.resolve(OrderHistory), { ssr: false });
