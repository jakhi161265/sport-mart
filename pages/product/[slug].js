import React, { useContext } from "react";
import {
  Typography,
  Grid,
  Card,
  Button,
  ListItem,
  List,
  Paper,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import Layout from "../../components/Layout";
import Image from "next/image";
import db from "../../utils/db";
import Product from "../../models/Product";
import axios from "axios";
import { Store } from "../../utils/Store";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import Rating from "@mui/material/Rating";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import AddShoppingCartRoundedIcon from "@mui/icons-material/AddShoppingCartRounded";
import dynamic from "next/dynamic";

const StyledGrid = styled(Grid)({
  marginTop: "1rem",
  marginBottom: "1rem",
});

function ProductDetail(props) {
  const router = useRouter();
  const { product } = props;
  const { state, dispatch } = useContext(Store);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleAddCart = async () => {
    closeSnackbar();
    const existItem = state.cart.cartItems.find((x) => x._id === product._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const { data } = await axios.get(`/api/products/${product._id}`);

    if (data.data.countInStock < quantity) {
      enqueueSnackbar("Sorry. Product is out of stock!!!!!!", {
        variant: "error",
      });
      return;
    }

    dispatch({ type: "CART_ADD_ITEM", payload: { ...product, quantity } });

    // if new product is added then show notification
    if (quantity === 1) {
      closeSnackbar();
      enqueueSnackbar("Product Added In The Cart", {
        variant: "success",
      });
    }

    // if quantity is greater than 1 then go to cart page
    if (quantity > 1) {
      router.push("/cart");
    }
  };

  if (!product) {
    return <div>Product Not Found!!!!</div>;
  }

  return (
    <Layout title={product.name} description={product.description}>
      <StyledGrid>
        <Button
          variant="outlined"
          startIcon={<ArrowBackRoundedIcon />}
          onClick={() => router.push("/")}
        >
          Back To Home
        </Button>
      </StyledGrid>

      <Grid container spacing={1}>
        <Grid item md={6} xs={12}>
          <Paper>
            <Image
              src={product.image}
              alt={product.name}
              width={640}
              height={640}
            ></Image>
          </Paper>
        </Grid>

        <Grid item md={3} xs={12}>
          <List>
            <ListItem>
              <Typography variant="h1" component="h1">
                {" "}
                {product.name}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                {" "}
                <b>Description:</b> {product.description}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                <b>Sport:</b> {product.category}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography>
                <b>Brand:</b> {product.brand}
              </Typography>
            </ListItem>
            <ListItem>
              <Rating name="read-only" value={product.rating} readOnly />
            </ListItem>
          </List>
        </Grid>

        <Grid item md={3} xs={12}>
          <Card>
            <List>
              <ListItem>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>Price</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>${product.price}</Typography>
                  </Grid>
                </Grid>
              </ListItem>

              <ListItem>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>Status</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      {product.countInStock > 0 ? "In Stock" : "Out of Stock"}
                    </Typography>
                  </Grid>
                </Grid>
              </ListItem>

              <ListItem>
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  onClick={() => handleAddCart()}
                  startIcon={<AddShoppingCartRoundedIcon />}
                >
                  Add To Cart
                </Button>
              </ListItem>
            </List>
          </Card>
        </Grid>
      </Grid>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { slug } = params;

  await db.connect();
  const product = await Product.findOne({ slug }).lean();
  await db.disconnect();

  return {
    props: {
      product: db.convertDocToObj(product),
    },
  };
}

export default dynamic(() => Promise.resolve(ProductDetail), { ssr: false });
