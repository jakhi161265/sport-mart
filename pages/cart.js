import React, { useContext } from "react";
import Layout from "../components/Layout";
import { Store } from "../utils/Store";
import {
  Grid,
  TableContainer,
  Table,
  Typography,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Link,
  Select,
  MenuItem,
  Button,
  Card,
  List,
  ListItem,
  IconButton,
  Tooltip,
} from "@mui/material";
import NextLink from "next/link";
import Image from "next/image";
import axios from "axios";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import StyledLink from "../components/StyledLink";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import cartImage from "../public/images/empty1.png";
import ShoppingCartCheckoutRoundedIcon from "@mui/icons-material/ShoppingCartCheckoutRounded";
import HighlightOffRoundedIcon from "@mui/icons-material/HighlightOffRounded";
import Rating from "@mui/material/Rating";
import { useSnackbar } from "notistack";

function CartDetails() {
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { state, dispatch } = useContext(Store);
  const {
    cart: { cartItems },
  } = state;

  const updateCartHandler = async (item, quantity) => {
    closeSnackbar();
    const { data } = await axios.get(`/api/products/${item._id}`);

    if (data?.data?.countInStock < quantity) {
      enqueueSnackbar("Sorry. Product is out of stock", {
        variant: "error",
      });
      return;
    }

    dispatch({ type: "CART_ADD_ITEM", payload: { ...item, quantity } });
  };

  const removeItemHandler = (item) => {
    closeSnackbar();
    enqueueSnackbar("Product Remove From The Cart", {
      variant: "error",
    });
    dispatch({ type: "CART_REMOVE_ITEM", payload: item });
  };

  const checkoutHandler = () => {
    router.push("/shipping");
  };

  return (
    <Layout title="Shopping Cart">
      {cartItems.length === 0 ? (
        <>
          <Grid container sx={{ textAlign: "center" }}>
            <Grid item xs={12} md={12}>
              <Image
                src={cartImage}
                alt={"cart"}
                width={550}
                height={300}
              ></Image>
            </Grid>
            <Grid xs={12} md={12}>
              <Button
                variant="contained"
                startIcon={<ArrowBackRoundedIcon />}
                onClick={() => router.push("/")}
              >
                Back To Home
              </Button>
            </Grid>
          </Grid>
        </>
      ) : (
        <>
          <Typography component="h1" variant="h1">
            Shopping Cart
          </Typography>

          <Grid container spacing={3}>
            <Grid item md={9} xs={12}>
              <TableContainer>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Image</TableCell>
                      <TableCell>Name</TableCell>
                      <TableCell>Rating</TableCell>
                      <TableCell align="right">Quantity</TableCell>
                      <TableCell align="right">Price</TableCell>
                      <TableCell align="right">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {cartItems.map((item) => (
                      <TableRow key={item._id}>
                        <TableCell>
                          <NextLink href={`/product/${item.slug}`} passHref>
                            <Link>
                              <Image
                                src={item.image}
                                alt={item.name}
                                width={50}
                                height={50}
                              ></Image>
                            </Link>
                          </NextLink>
                        </TableCell>

                        <TableCell>
                          <NextLink href={`/product/${item.slug}`} passHref>
                            <StyledLink>
                              <Typography>{item.name}</Typography>
                            </StyledLink>
                          </NextLink>
                        </TableCell>

                        <TableCell>
                          <Rating
                            name="read-only"
                            value={item.rating}
                            readOnly
                          />
                        </TableCell>

                        <TableCell align="right">
                          <Select
                            size="small"
                            value={item.quantity}
                            onChange={(e) =>
                              updateCartHandler(item, e.target.value)
                            }
                          >
                            {[...Array(item.countInStock).keys()].map((x) => (
                              <MenuItem key={x + 1} value={x + 1}>
                                {x + 1}
                              </MenuItem>
                            ))}
                          </Select>
                        </TableCell>
                        <TableCell align="right">${item.price}</TableCell>
                        <TableCell align="right">
                          <Tooltip title="Remove Item">
                            <IconButton
                              sx={{ color: "red" }}
                              onClick={() => removeItemHandler(item)}
                            >
                              <HighlightOffRoundedIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            <Grid item md={3} xs={12}>
              <Card>
                <List>
                  <ListItem>
                    <Typography variant="h3">
                      Subtotal ({cartItems.reduce((a, c) => a + c.quantity, 0)}{" "}
                      items) : $
                      {cartItems.reduce((a, c) => a + c.quantity * c.price, 0)}
                    </Typography>
                  </ListItem>
                  <ListItem>
                    <Button
                      onClick={checkoutHandler}
                      variant="contained"
                      color="primary"
                      fullWidth
                      startIcon={<ShoppingCartCheckoutRoundedIcon />}
                    >
                      Check Out
                    </Button>
                  </ListItem>
                </List>
              </Card>
            </Grid>
          </Grid>
        </>
      )}
    </Layout>
  );
}

export default dynamic(() => Promise.resolve(CartDetails), { ssr: false });
