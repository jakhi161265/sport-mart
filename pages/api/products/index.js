import db from "../../../utils/db";
import Product from "../../../models/Product";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "GET":
      try {
        await db.connect();
        const products = await Product.find({});
        await db.disconnect();
        res.status(200).json({ success: true, data: products });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
}
