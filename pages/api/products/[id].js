import db from "../../../utils/db";
import Product from "../../../models/Product";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "GET":
      try {
        await db.connect();
        const product = await Product.findById(req.query.id);
        await db.disconnect();
        res.status(200).json({ success: true, data: product });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
}
