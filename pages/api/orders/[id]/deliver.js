import db from "../../../../utils/db";
import Order from "../../../../models/Order";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "PUT":
      try {
        await db.connect();
        const order = await Order.findById(req.query.id);
        if (order) {
          order.isDelivered = true;
          order.isPaid = true;
          order.deliveredAt = Date.now();
          order.paidAt = Date.now();
          const deliveredOrder = await order.save();
          await db.disconnect();
          res.send({ message: "order delivered", order: deliveredOrder });
        } else {
          await db.disconnect();
          res.status(404).send({ message: "order not found" });
        }
      } catch (error) {
        res.status(400).json({ success: false , message:"Fail to make delivery"});
      }
      break;

    default:
      res.status(400).json({ success: false, message:"Others Error" });
      break;
  }
}
