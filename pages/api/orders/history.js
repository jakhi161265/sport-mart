import db from "../../../utils/db";
import Order from "../../../models/Order";
import jwt from "jsonwebtoken";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "GET":
      try {
        const { authorization } = req.headers;
        if (authorization) {
          // Bearer xxx => xxx
          const token = authorization.slice(7, authorization.length);
          jwt.verify(token, process.env.JWT_SECRET, (err, decode) => {
            if (err) {
              res.status(401).send({ message: "Token is not valid" });
            } else {
              req.user = decode; // given userInfo object
            }
          });
        } else {
          res.status(401).send({ message: "Token is not given" });
        }

        await db.connect();
        let filter = {};
        if (!req.user.isAdmin) {
          filter = { user: req.user._id };
        }
        const orders = await Order.find(filter);
        await db.disconnect();
        res.status(201).send(orders);
      } catch (error) {
        console.log(error);
        res.status(400).json({ success: false, message: error.message });
      }
      break;

    default:
      res.status(400).json({ success: false, message: "Default" });
      break;
  }
}
