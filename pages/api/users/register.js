import db from "../../../utils/db";
import User from "../../../models/User";
import bcrypt from "bcryptjs";
import { signToken } from "../../../utils/auth";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "POST":
      try {
        await db.connect();
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password),
          isAdmin: false,
        });

        const user = await newUser.save();
        await db.disconnect();

        const token = signToken(user);
        res.status(200).send({
          token,
          _id: user._id,
          name: user.name,
          email: user.email,
          isAdmin: user.isAdmin,
        });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
}
