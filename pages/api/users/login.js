import db from "../../../utils/db";
import User from "../../../models/User";
import bcrypt from "bcryptjs";
import { signToken } from "../../../utils/auth";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "POST":
      try {
        await db.connect();
        const user = await User.findOne({ email: req.body.email });
        await db.disconnect();

        if (user && bcrypt.compareSync(req.body.password, user.password)) {
          const token = signToken(user);
          res.status(200).send({
            token,
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
          });
        } else {
          res
            .status(401)
            .send({ success: false, message: "Invalid user or password"});
        }
      } catch (error) {
        res.status(400).json({ success: false, message: "Not Found" });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
}
