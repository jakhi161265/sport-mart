import User from "../../../models/User";
import db from "../../../utils/db";
import { signToken } from "../../../utils/auth";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";

export default async function handler(req, res) {
  const { method } = req;

  await db.connect();

  switch (method) {
    case "PUT":
      try {
        const { authorization } = req.headers;
        if (authorization) {
          // Bearer xxx => xxx
          const token = authorization.slice(7, authorization.length);
          jwt.verify(token, process.env.JWT_SECRET, (err, decode) => {
            if (err) {
              res.status(401).send({ message: "Token is not valid" });
            } else {
              req.user = decode; // given userInfo object
            }
          });
        } else {
          res.status(401).send({ message: "Token is not suppiled" });
        }

        await db.connect();
        const user = await User.findById(req.user._id);
        user.name = req.body.name;
        user.email = req.body.email;
        user.password = req.body.password
          ? bcrypt.hashSync(req.body.password)
          : user.password;
        await user.save();
        await db.disconnect();

        const token = signToken(user);
        res.send({
          token,
          _id: user._id,
          name: user.name,
          email: user.email,
          isAdmin: user.isAdmin,
        });
      } catch (error) {
        console.log(error);
        res.status(400).json({ success: false, message: error.message });
      }
      break;

    default:
      res.status(400).json({ success: false, message: "Default" });
      break;
  }
}
