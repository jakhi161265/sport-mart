import db from "../../utils/db";
import Product from "../../models/Product";
import data from "../../utils/data";
import User from "../../models/User";

export default async function handler(req, res) {
  await db.connect();

  try {
    await db.connect();

    await Product.deleteMany();
    await Product.insertMany(data.products);

    await User.deleteMany();
    await User.insertMany(data.users);

    await db.disconnect();

    res.status(200).json({ success: true, message: "Seeded Successfully" });
  } catch (error) {
    console.log(error.message)
    res.status(400).json({ success: false, message:"Fail To Save Data" });
  }
}
