import { StoreProvider } from "../utils/Store";
import { CacheProvider } from "@emotion/react";
import { ThemeProvider } from "@mui/system";
import createEmotionCache from "../utils/createEmotionCache";
import { theme } from "../utils/theme";
import { SnackbarProvider } from "notistack";

const clientSideEmotionCache = createEmotionCache();

function MyApp(props) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <SnackbarProvider
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <ThemeProvider theme={theme}>
          <StoreProvider>
            <Component {...pageProps} />
          </StoreProvider>
        </ThemeProvider>
      </SnackbarProvider>
    </CacheProvider>
  );
}

export default MyApp;
