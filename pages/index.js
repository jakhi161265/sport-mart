import Layout from "../components/Layout";
import { Grid } from "@mui/material";
import db from "../utils/db";
import Product from "../models/Product";
import { useContext } from "react";
import { Store } from "../utils/Store";
import axios from "axios";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import ProductItem from "../components/ProductItem";
import Carousel from "react-material-ui-carousel";
import sliderData from "../utils/sliderData";
import CarouselItem from "../components/CarouselItem";
import dynamic from "next/dynamic";


function Home(props) {
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { products } = props;
  const { state, dispatch } = useContext(Store);

  const handleAddCart = async (product) => {
    closeSnackbar();
    const existItem = state.cart.cartItems.find((x) => x._id === product._id);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const { data } = await axios.get(`/api/products/${product._id}`);

    if (data.data.countInStock < quantity) {
      enqueueSnackbar("Sorry. Product is out of stock!!!!!!", {
        variant: "error",
      });
      return;
    }

    dispatch({ type: "CART_ADD_ITEM", payload: { ...product, quantity } });

    // if new product is added then show notification
    if (quantity === 1) {
      closeSnackbar();
      enqueueSnackbar("Product Added In The Cart", {
        variant: "success",
      });
    }

    // if quantity is greater than 1 then go to cart page
    if (quantity > 1) {
      router.push("/cart");
    }
  };

  return (
    <div>
      <main>
        <Layout>
          <Carousel animation="slide" sx={{ height: "30rem" }}>
            {sliderData.map((item, i) => (
              <CarouselItem key={i} item={item} />
            ))}
          </Carousel>

          <h2>Products</h2>
          <Grid container spacing={3} rowSpacing={5}>
            {products?.map((product) => (
              <Grid item md={3} key={product.name}>
                <ProductItem
                  product={product}
                  handleAddCart={handleAddCart}
                ></ProductItem>
              </Grid>
            ))}
          </Grid>
        </Layout>
      </main>
    </div>
  );
}

export async function getServerSideProps() {
  await db.connect();
  const products = await Product.find({}).lean().sort({
    rating: -1,
  });
  await db.disconnect();

  return {
    props: {
      products: products?.map(db.convertDocToObj),
    },
  };
}

export default dynamic(() => Promise.resolve(Home), { ssr: false });
