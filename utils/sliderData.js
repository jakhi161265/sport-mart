const sliderData = [
  {
    image: "/images/slider-1.webp",
    name: "image 1",
  },
  {
    image: "/images/slider-3.jpg",
    name: "image 3",
  },
  {
    image: "/images/slider-2.webp",
    name: "image 2",
  },
  {
    image: "/images/slider-4.webp",
    name: "image 4",
  },
];

export default sliderData;
