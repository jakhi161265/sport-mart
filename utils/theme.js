import { createTheme } from "@mui/material/styles";

// main: "#651fff", // purple
export const theme = createTheme({
  palette: {
    type: "light",
    primary: {
      main: "#358856", // green
    },
    secondary: {
      main: "#ec407a",
    },
    success: {
      main: "#519259",
    },
    info: {
      main: "#000000",
    },
    error: {
      main: "#F13A4B",
    },
  },
  typography: {
    h1: { fontSize: "1.6rem", fontWeight: 400, margin: "1rem 0" },
    h2: { fontSize: "1.4rem", fontWeight: 400, margin: "1rem 0" },
    h3: { fontSize: "1.2rem", fontWeight: 400, margin: "0rem 0" },
    h6: { fontSize: "1.4rem", fontWeight: 500, margin: "0rem 0" },
  },
});
